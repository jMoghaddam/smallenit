import java.io.*;
import java.util.*;

public class Main {

    public static void main(String[] args) {
        FrequencyList frequencyList = new FrequencyList();
        MinHeap minHeap = new MinHeap(); // Priority queue

        try {
            // File path
            File file = new File("myFile.txt");
            FileReader fileReader = new FileReader(file);
            BufferedReader bufferedReader = new BufferedReader(fileReader);

            // read file char by char and add finf frequencies of chars in O(n)
            int c = 0;
            while((c = bufferedReader.read()) != -1)
            {
                char character = (char) c;
                frequencyList.addFrequency(character);
            }

            // Add frequencies to min heap ( priority queue )
            for (int i = 0; i < 256; i++) {
                if (frequencyList.frequencies[i] != 0) minHeap.insert(new Node((char) i, frequencyList.frequencies[i]));
            }

            // Generate Huffman tree
            HuffmanTree huffmanTree = new HuffmanTree();

            while (minHeap.count > 1) {
                Node n1 = minHeap.remove();
                Node n2 = minHeap.remove();
                Node result = huffmanTree.insertNodes(n1, n2);
                minHeap.insert(result);
            }

            ArrayList<Node> list = new ArrayList<>(); // List of all nodes
            huffmanTree.traverse(huffmanTree.head, 0, list); // put all nodes in the list along with huffman code

            // Sort the list for canonical form, by huffman code length and then lexicographically
            Collections.sort(list, new Comparator() {

                public int compare(Object o1, Object o2) {
                    String code1 = ((Node) o1).code;
                    String code2 = ((Node) o2).code;

                    if (code1.length() < code2.length()) {
                        return -1;
                    } else if (code1.length() > code2.length()) {
                        return 1;
                    }

                    int distance = ((int)((Node) o1).c) - ((int)((Node) o2).c);

                    if (distance > 0) return 1;
                    else if (distance < 0) return -1;
                    else return 0;
                }});

//            frequencyList.printFrequencies();

//            for (Node n : list) {
//                System.out.println(n.c + " - " + n.code.length());
//            }

            // Generate canonical Huffman codes
            for (int i = 0; i < list.get(0).code.length(); i++) {
                list.get(0).canonicalCode[list.get(0).canonicalLength] = 0;
                list.get(0).canonicalLength++;
            }

            int i = 1;
            int[] temp = new int[256];
            while (i < list.size()) {
                if (list.get(i).code.length() == list.get(i - 1).code.length()) {
                    int k = list.get(i - 1).canonicalLength - 1;
                    while (k >= 0) {
                        if (list.get(i - 1).canonicalCode[k] == 1) {
                            temp[k] = 0;
                            k--;
                        } else {
                            temp[k] = 1;
                            break;
                        }
                    }
                    for (int m = k - 1; m >= 0; m--) temp[m] = list.get(i - 1).canonicalCode[m];

                    for (int j = 0; j < list.get(i - 1).canonicalLength; j++) {
                        list.get(i).canonicalCode[list.get(i).canonicalLength] = temp[j];
                        list.get(i).canonicalLength++;
                    }
                } else {
                    int k = list.get(i - 1).canonicalLength - 1;
                    while (k >= 0) {
                        if (list.get(i - 1).canonicalCode[k] == 1) {
                            temp[k] = 0;
                            k--;
                        } else {
                            temp[k] = 1;
                            break;
                        }
                    }
                    for (int m = k - 1; m >= 0; m--) temp[m] = list.get(i - 1).canonicalCode[m];

                    for (int j = 0; j < list.get(i - 1).canonicalLength; j++) {
                        list.get(i).canonicalCode[j] = temp[j];
                        list.get(i).canonicalLength++;
                    }
                    for (int m = 0; m < list.get(i).code.length() - list.get(i - 1).code.length(); m++) {
                        list.get(i).canonicalCode[list.get(i).canonicalLength] = 0;
                        list.get(i).canonicalLength++;
                    }
                }
                i++;
            }

//            for (Node n : list) {
//                System.out.print(n.c + " - ");
//                for (int j = 0; j < n.canonicalLength; j++) {
//                    System.out.print(n.canonicalCode[j]);
//                }
//                System.out.println();
//            }

            // Output
            FileOutputStream fileOutputStream = new FileOutputStream("compressed.bin");
            ObjectOutputStream os = new ObjectOutputStream(fileOutputStream);

            // How many chars (for decoder)
            os.writeInt(list.size());

            // access nodes directly without search by char
            Node[] map = new Node[256];

            // write code lengths (for decoder)
            for (Node n : list) {
                os.writeChar(n.c);
                os.writeInt(n.code.length());
                map[(int )n.c] = n;
            }

            // Replace each char by its canonical code
            c = 0;
            int bitCounter = 0;
            byte b = 0;
            int x = 7;
            fileReader = new FileReader(file);
            bufferedReader = new BufferedReader(fileReader);
            while((c = bufferedReader.read()) != -1)
            {
                for (int p = 0; p < map[c].canonicalLength; p++) {
                    if (map[c].canonicalCode[p] == 1) b |= 1 << x;
                    else b &= ~(1 << x);
                    x--;
                    bitCounter++;
                    if (x == -1) {
                        x = 7;
                        os.writeByte(b);
                    }
                }
            }

//            int leftOver = 0;
//            if (x != 7) {
//                while (x != -1) {
//                    b &= ~(1 << x);
//                    x--;
//                    leftOver++;
//                }
//                os.writeByte(b);
//            }
//
//            os.writeByte(8 - leftOver);

            os.close();
        } catch (FileNotFoundException e) {
            System.out.println("no file found");
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
