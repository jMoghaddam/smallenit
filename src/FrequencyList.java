public class FrequencyList {

    int[] frequencies = new int[256];

    public void addFrequency(char c) {
        frequencies[(int) c]++;
    }

    public void printFrequencies() {
        for (int i = 0; i < 255; i++) {
            if (frequencies[i] != 0) System.out.println( (i) + " " + frequencies[i]);
        }
    }

}
