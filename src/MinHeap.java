public class MinHeap {
    Node[] queue = new Node[256];
    int count = 0;

    void insert(Node cf) {
        int i = count;
        while (i > 0 && cf.f < queue[i/2].f) {
            queue[i] = queue[i / 2];
            i /= 2;
        }
        queue[i] = cf;
        count++;
    }

    Node remove() {
        Node head = queue[0];
        queue[0] = queue[count - 1];
        int i = 0;
        int j = 2*i + 1;

        while (j < count - 1) {
            if (queue[j].f > queue[j + 1].f) j++;
            if (queue[i].f > queue[j].f) {
                Node temp = queue[i];
                queue[i] = queue[j];
                queue[j] = temp;

                i = j;
                j *= 2;
            } else break;
        }

        count--;
        return head;
    }

}
