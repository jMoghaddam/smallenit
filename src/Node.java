public class Node {
    Node left;
    char c;
    int f;
    Node right;
    String code = "";
    int[] canonicalCode = new int[256];
    int canonicalLength;

    public Node(Node left, char c, int f, Node right) {
        this.left = left;
        this.c = c;
        this.f = f;
        this.right = right;
    }

    public Node(Node left, int f, Node right) {
        this.left = left;
        this.f = f;
        this.right = right;
    }

    public Node(char c, int f) {
        this.c = c;
        this.f = f;
    }

}
