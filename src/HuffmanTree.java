import java.util.ArrayList;
import java.util.List;

public class HuffmanTree {
    Node head;
    int[] tempCode = new int[100];

    public Node insertNodes(Node n1, Node n2) {
        head = new Node(n1, n1.f + n2.f, n2);
        return head;
    }

    public void traverse(Node n, int top, ArrayList<Node> list) {

        if (n.left != null) {
            tempCode[top] = 0;
            traverse(n.left, top + 1, list);
        }

        if (n.right != null) {
            tempCode[top] = 1;
            traverse(n.right, top + 1, list);
        }

        if (n.c != Character.MIN_VALUE) {
            StringBuilder sb = new StringBuilder();
            for (int i = 0; i < top; i++) {
                sb.append(tempCode[i]);
            }
            n.code = sb.toString();
            list.add(n);
        }

    }
}
